---
layout: home
title: Start
permalink: /
nav_order: 1
---

<img src="/welcome.small.jpeg" style="width: 100%" alt="Die Plattform Pappelheim im #WiWaBleibt">

# Besetzung des Wilden Walds in Hamburg

Der Wilde Wald ist ein wundergrüner Wald mitten im 💚 von Hamburg. 🌳🌿🍂🌱
Dieser Wald soll zerstört werden, doch wir sorgen dafür, dass der #WiWaBleibt.
Indem wir ihn seit Oktober 2019 besetzen. Am 4.12.2020 lief das städtische
Ultimatum zu gehen aus. Wir bleiben.

Der fünfte Jahrestag des Pariser Klimaabkommens ist verstrichen und wir
befinden uns noch immer weit davon entfernt Politik zu führen, die mit dem
1,5-Grad-Ziel übereinstimmt. 

Stattdessen werden Projekte finanziert, die schon lange nicht mehr zeitgemäß
sind. So wird im Moment in Hessen ein Jahrhunderte alter Mischwald gefällt und
wozu? Für eine Autobahn. Verkehrswende geht anders! Daher streiken wir. Im
[Danni](https://waldstattasphalt.blackblogs.org/) und jetzt auch hier! Kommt
gerne dazu, auch Bodensupport ist immer gerne gesehen! 💪

<img src="/icons/telegram.svg" style="width: 1em; height: 1em">
**[Infokanal auf Telegram](https://t.me/WiWa_bleibt)**
(nur wenig Nachrichten; es gibt auch eine
[Diskussionsgruppe](https://t.me/WiWaBleibtaustausch) zum direkten Kontakt)

<img src="/icons/instagram.svg" style="width: 1em; height: 1em">
**[Instagram @WiWaBleibt](https://www.instagram.com/WiWaBleibt)**

<img src="/icons/twitter.svg" style="width: 1em; height: 1em">
**Twitter:**
[@WiWa_Bleibt](https://twitter.com/WiWa_Bleibt),
[@asen_im](https://twitter.com/asen_im),
[@pappelheimer](https://twitter.com/pappelheimer)

**Weitere Hintergründe auf den [Seiten der
Bürger\*inneninitiative](https://waldretter.de/) zur Rettung
des WiWa**

<!--
[Pressemitteilungen](/pressemitteilungen){: .btn .btn-green }
[Fotos](https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/){: .btn .btn-green }
-->


## Ort

Wilder Wald Wilhelmsburg (nächste Adresse: Vogelhüttendeich 117A, 21107
Hamburg)

Komm vorbei! Wenn du magst, bring etwas von unserer [Wunschliste](/wunschliste)
mit :-)


## Impressum

Diese Webseite wird geplegt von: Ingo Blechschmidt, Arberstr. 5, 86179
Augsburg, +49 176 95110311, iblech@web.de. In Deutschland gibt es [noch viele
weitere Klimacamps](https://klimacamp.eu/) und Solibaumhäuser für den
[Danni](https://waldstattasphalt.blackblogs.org/).

<script data-no-instant>
  {% include instantclick.min.js %}
  InstantClick.init();
</script>
