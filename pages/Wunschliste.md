---
layout: page
title: Wunschliste
permalink: /wunschliste/
nav_order: 2
---

# Wunschliste

*zuletzt aktualisiert am 22.12.2020*

## Lebensmittel/Kochen
- veganes haltbares Essen
- Töpfe
- Ofen
- Kocher


## Bauen
- Planen
- Bauholz (Balken, Bretter, Paletten, Baumstämme)
- Fenster
- Solarpanel
- Türen
- Schrauben ab 6 cm
- Werkzeug
- Wellblech
- 6mm, 12mm & 14mm Polyprop
- Hammer
- Nägel alle größen


## Klettern

- Karabiner
- Bandschlingen
- 6mm Reepschnur
- Fallnetze
- Kletterseile


## Schlafen/chillen
- Isomatten
- Schlafsäcke
- Sofas
- Zelte


## Sonstiges

- Sekundenkleber
- Glitzer
- Batterien
- Kerzen
- flüssiges Desinfektionsmittel
- Bemalte Banner
- was euch in den Sinn kommt & seid kreativ 😉
